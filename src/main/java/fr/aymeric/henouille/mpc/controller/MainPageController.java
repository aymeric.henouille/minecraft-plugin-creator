package fr.aymeric.henouille.mpc.controller;

import fr.aymeric.henouille.mpc.MinecraftPluginCreator;
import fr.aymeric.henouille.mpc.project.Project;
import fr.aymeric.henouille.mpc.project.ProjectGenerator;
import fr.aymeric.henouille.mpc.stage.StageInjector;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class MainPageController implements Initializable, StageInjector {

    private Stage stage;

    @FXML
    private TextField name;
    @FXML
    private TextField domain;
    @FXML
    private TextField path;
    @FXML
    private TextField author;
    @FXML
    private ChoiceBox<String> version;
    @FXML
    private Label appVersion;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (String versions : MinecraftPluginCreator.getVersionManager().getSpigotVersion()) {
            version.getItems().add(versions);
        }
        version.setValue(MinecraftPluginCreator.getVersionManager().getSpigotVersion()[0]);
        appVersion.setText(MinecraftPluginCreator.getVersionManager().getAppVersion());
    }

    @FXML
    public void openFileSelectorDialogue() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Select project path");
        File selected = chooser.showDialog(stage.getOwner());
        if (selected != null)
            path.setText(selected.getAbsolutePath());
    }

    @FXML
    public void close() {
        stage.close();
    }

    @FXML
    public void onGenerate() {
        String name = this.name.getText();
        String domain = this.domain.getText();
        String path = this.path.getText();
        String version = this.version.getValue();
        String author = this.author.getText();

        Project project = new Project(name, domain, path, version, author);
        ProjectGenerator.generate(project);
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public Stage getStage() {
        return stage;
    }
}
