package fr.aymeric.henouille.mpc.project;

import fr.aymeric.henouille.mpc.utils.AssetManager;
import fr.aymeric.henouille.mpc.utils.FileInterpreter;
import fr.aymeric.henouille.mpc.utils.FileUtils;

import java.io.*;

public class ProjectGenerator {

    private AssetManager assetManager;
    private Project project;

    public ProjectGenerator(Project project) {
        this.project = project;
        this.assetManager = new AssetManager();
    }

    private void setUpGradleWrapper() {
        File gradle = assetManager.getFile("project/gradle.zip");
        File dest = new File(project.getPath());
        FileUtils.unZip(gradle.getAbsolutePath(), dest.getAbsolutePath());
    }

    private void setUpSrcCode() {
        File projectPath = new File(project.getPath());

        String mainPackagePath = projectPath.getAbsolutePath() + "/src/main/" + project.getPackage().replace(".", "/") + "/";
        String testPackagePath = projectPath.getAbsolutePath() + "/src/test/" + project.getTestPackage().replace(".", "/") + "/";

        File main = new File( mainPackagePath + project.getMainClassName() + ".java");
        File manager = new File(mainPackagePath + "manager/ListenerManager.java");
        File listener = new File(mainPackagePath + "listener/PlayerJoinListener.java");

        File testMain = new File(testPackagePath + "Test" + project.getMainClassName() +".java");
        File testListener = new File(testPackagePath + "/listener/TestPlayerJoinListener.java");

        System.out.println("create : " + main);
        main.getParentFile().mkdirs();
        System.out.println("create : " + manager);
        manager.getParentFile().mkdirs();
        System.out.println("create : " + listener);
        listener.getParentFile().mkdirs();
        System.out.println("create : " + testMain);
        testMain.getParentFile().mkdirs();
        System.out.println("create : " + testListener);
        testListener.getParentFile().mkdirs();

        try (FileOutputStream fis = new FileOutputStream(main) ;
             BufferedOutputStream bis = new BufferedOutputStream(fis) ;
             PrintWriter writer = new PrintWriter(bis)) {

            main.createNewFile();
            FileInterpreter fileInterpreter = new FileInterpreter(assetManager.getFile("project/projects/main"));
            writer.println(this.generateFileContent(fileInterpreter));

        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fis = new FileOutputStream(manager) ;
             BufferedOutputStream bis = new BufferedOutputStream(fis) ;
             PrintWriter writer = new PrintWriter(bis)) {

            manager.createNewFile();
            FileInterpreter managerFileInterpreter = new FileInterpreter(assetManager.getFile("project/projects/events"));
            writer.println(this.generateFileContent(managerFileInterpreter));

        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fis = new FileOutputStream(listener) ;
             BufferedOutputStream bis = new BufferedOutputStream(fis) ;
             PrintWriter writer = new PrintWriter(bis)) {

            listener.createNewFile();
            FileInterpreter listenerFileInterpreter = new FileInterpreter(assetManager.getFile("project/projects/listener"));
            writer.println(this.generateFileContent(listenerFileInterpreter));

        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fis = new FileOutputStream(testMain) ;
             BufferedOutputStream bis = new BufferedOutputStream(fis) ;
             PrintWriter writer = new PrintWriter(bis)) {

            listener.createNewFile();
            FileInterpreter listenerFileInterpreter = new FileInterpreter(assetManager.getFile("project/projects/test_main"));
            writer.println(this.generateFileContent(listenerFileInterpreter));

        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fis = new FileOutputStream(testListener) ;
             BufferedOutputStream bis = new BufferedOutputStream(fis) ;
             PrintWriter writer = new PrintWriter(bis)) {

            listener.createNewFile();
            FileInterpreter listenerFileInterpreter = new FileInterpreter(assetManager.getFile("project/projects/test_listener"));
            writer.println(this.generateFileContent(listenerFileInterpreter));

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void setUpProperties() {
        File properties = new File(project.getPath() + "/gradle.properties");

        try (FileOutputStream fis = new FileOutputStream(properties) ;
             BufferedOutputStream bis = new BufferedOutputStream(fis) ;
             PrintWriter writer = new PrintWriter(bis)) {

            System.out.println("create : " + properties);
            properties.createNewFile();
            FileInterpreter fileInterpreter = new FileInterpreter(assetManager.getFile("project/properties"));
            writer.println(this.generateFileContent(fileInterpreter));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUpPluginYml() {
        File plugin = new File(project.getPath() + "/src/main/plugin.yml");
        try (FileOutputStream fis = new FileOutputStream(plugin) ;
             BufferedOutputStream bis = new BufferedOutputStream(fis) ;
             PrintWriter writer = new PrintWriter(bis)) {
            System.out.println("create : " + plugin);
            plugin.createNewFile();
            FileInterpreter fileInterpreter = new FileInterpreter(assetManager.getFile("project/plugin"));

            writer.println(this.generateFileContent(fileInterpreter));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void generate(Project project) {
        ProjectGenerator generator = new ProjectGenerator(project);
        generator.setUpGradleWrapper();
        generator.setUpSrcCode();
        generator.setUpProperties();
        generator.setUpPluginYml();
    }


    private String generateFileContent(FileInterpreter fileInterpreter) {
        fileInterpreter.addVariable("package_name", project.getPackage());
        fileInterpreter.addVariable("test_package_name", project.getTestPackage());
        fileInterpreter.addVariable("class_name", project.getMainClassName());
        fileInterpreter.addVariable("plugin_name", project.getName());
        fileInterpreter.addVariable("minecraft_version", project.getMinecraftVersion());
        fileInterpreter.addVariable("author", project.getAuthor());
        String minecraftVersion = project.getMinecraftVersion();
        fileInterpreter.addVariable("api_version", minecraftVersion.substring(0, minecraftVersion.lastIndexOf(".")));

        return fileInterpreter.generate();
    }


}
