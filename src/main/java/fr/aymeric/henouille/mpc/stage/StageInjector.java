package fr.aymeric.henouille.mpc.stage;

import javafx.stage.Stage;

public interface StageInjector {

    void setStage(Stage stage);
    Stage getStage();

}
