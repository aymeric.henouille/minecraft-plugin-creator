package fr.aymeric.henouille.mpc.utils;

public class VersionManager {

    private String appVersion;
    private String[] spigotVersion;

    public VersionManager() {
        AssetManager assetManager = new AssetManager();
        this.appVersion = new FileInterpreter(assetManager.getFile("version/app.version")).generate();
        this.spigotVersion = new FileInterpreter(assetManager.getFile("version/spigot.version")).generate()
                .split("\n");
    }

    public String getAppVersion() {
        return appVersion;
    }

    public String[] getSpigotVersion() {
        return spigotVersion;
    }

}
