package fr.aymeric.henouille.mpc.utils;

import java.io.File;
import java.net.URISyntaxException;

public class AssetManager {


    public File getFile(String name) {
        return new File(getAssetFile().getAbsolutePath() + "/" + name);
    }

    public File getAssetFile() {
        try {
            return new File(new File(AssetManager.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getParent() + "/assets");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

}
