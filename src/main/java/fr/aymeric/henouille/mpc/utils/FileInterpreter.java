package fr.aymeric.henouille.mpc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class FileInterpreter {

    public static final String DEFAULT_VARIABLE_DELIMITER = "%";

    protected final File file;

    protected List<String> content;
    protected HashMap<String, String> variables;

    protected String variableDelimiter;

    public FileInterpreter(String file) {
        this(new File(file));
    }

    public FileInterpreter(File file) {
        this.file = file;
        variableDelimiter = DEFAULT_VARIABLE_DELIMITER;
        variables = new HashMap<String, String>();
        read();
    }

    public void read() {
        content = new ArrayList<String>();

        try (
                FileInputStream fis = new FileInputStream(file);
                BufferedInputStream bos = new BufferedInputStream(fis);
                Scanner scanner = new Scanner(bos);
        ) {

            while(scanner.hasNext()) {
                content.add(scanner.nextLine());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String generate() {
        String index = "";
        for (String line : content) {
            for (String key : variables.keySet()) {
                line = line.replace(variableDelimiter + key + variableDelimiter, variables.get(key));
            }
            index+=line+"\n";
        }
        return index;
    }

    public void addVariable(String name, String value) {
        variables.putIfAbsent(name, value);
    }

    public void setVariableDelimiter(String variableDelimiter) {
        this.variableDelimiter = variableDelimiter;
    }

    public String getVariableDelimiter() {
        return variableDelimiter;
    }

    public List<String> getContent() {
        return content;
    }

    public File getFile() {
        return file;
    }

}